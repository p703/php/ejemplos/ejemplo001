<?php


$letras=[
    'T',
    'R',
    'W',
    'A',
    'G',
    'M',
    'Y',
    'F',
    'P',
    'D',
    'X',
    'B',
    'N',
    'J',
    'Z',
    'S',
    'Q',
    'V',
    'H',
    'L',
    'C',
    'K',
    'E',
    'T'
    ];

$dni=20211818;
        
$letra="Q";

if ($dni<0 || $dni>99999999){
    echo "Numero no valido";
}else { 
    $indice=$dni%23; //esto es la posicion donde esta la letra calculada
    $letracalculada=$letras[$indice];

    if($letra==$letracalculada){
        echo "Letra correcta";
    }else {
        echo "Letra incorrecta";
    }
}


