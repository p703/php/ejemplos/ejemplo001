<?php

$dia=6;

// Con if
if ($dia==1){
    echo "Lunes";
}elseif ($dia==2){
    echo "Martes";
}elseif ($dia==3){
    echo "Miercoles";
}elseif ($dia==4){
    echo "Jueves";
}elseif ($dia==5){
    echo "Viernes";
}elseif ($dia==6){
    echo "Sabado";
}elseif ($dia==7){
    echo "Domingo";
};
    
// Con swich
switch ($dia) {
            case 1:
                $salida="Lunes";
                break;
            case 2:
                $salida="Martes";
                break;
            case 3:
                $salida="Miercoles";
                break;
            case 4:
                $salida="Jueves";
                break;
            case 5:
                $salida="Viernes";
                break;
            case 6:
                $salida="Sabado";
                break;
            case 7:
                $salida="Domingo";
                break;
            default:
                $salida="No es un dia de la semana";
        }
        echo $salida;

        
// con array
$dias=[
    "Lunes", 
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sabado",
    "Domingo"
];


echo $dias[$dia-1];