<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .salida{
                border:1px solid black;
                width:80px;
                height:40px;
                line-height: 40px;
                text-align: center;
                display:inline-block;
            }
            div{
                color:blue;
                
            }
        </style>
    </head>
    <body>
       <?php
            $ancho="400px";
            $alto="400px";
            
            $dados=[1,2,3,4,5,6];
            
            $numeroFotos=count($dados);
            $indice= mt_rand(0,$numeroFotos-1);
            $foto=$dados[$indice];
            
            $numeroFotos=count($dados);
            $indice= mt_rand(0,$numeroFotos-1);
            $foto1=$dados[$indice];
            
            $total=$foto+$foto1;
            
            /**
             * Otra forma de poner las variables
             * $dado1=mt_rand(1,6);
             * $dado2=mt_rand(1,6);
             */
        ?>
        <div style="width:<?= ((int)$ancho)*2 ?>px;">
        <img style="width:<?= $ancho ?>; height: <?= $alto ?>;" src="imgs/<?= $foto ?>.svg" alt="una foto"/>
        <img style="width:<?= $ancho ?>; height: <?= $alto ?>;" src="imgs/<?= $foto1 ?>.svg" alt="una foto"/>
        </div>
        <div>
            Total <div class="salida"><?= $total ?></div>
            
        </div>
    </body>
</html>