<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $contador=0;
        $sumaTiradas=[];
        
        for($c=0;$c<10;$c++){
            $d1= rand(1,6);
            $d2= rand(1,6);
            $sumaTiradas[]=$d1+$d2;
        ?>
        <div>
            <div class="dados">
                <img src="imgs/<?= $d1 ?>.svg" alt="dado1"/>
                <img src="imgs/<?= $d2 ?>.svg" alt="dado2"/>  
            </div>
            <div class="total">Total: <span> <?= $sumaTiradas[$contador++] ?></span></div>
        </div>
        
        <?php   
        }
        
        var_dump($sumaTiradas);
        ?>
        <div class="mayor">
            La tirada mayor ha sido de <?= max($sumaTiradas) ?>
        </div>
    </body>
</html>