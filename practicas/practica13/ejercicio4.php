<!DOCTYPE html>
<?php
function ejercicio4a($numero1,$numero2){
  echo $numero1+$numero2 . "<br/>";  
}

function ejercicio4b($numero1,$numero2,$numero3){
    echo $numero1+$numero2+$numero3 . "<br/>";
}

/**
 * podemos usar argumentos opcionales para solo hacer una funcion
 */

function ejercicio4($numero1=0,$numero2=0,$numero3=0){
    $resultado=$numero1+$numero2+$numero3;
    echo "<br/>{$resultado}<br/>";
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio4a(5,7);
        ejercicio4b(5,7,1);
        ejercicio4();
        ?>
    </body>
</html>
