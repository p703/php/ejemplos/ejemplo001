<!DOCTYPE html>
<?php
function ejercicio13($colores){
    foreach ($colores as $color) {
       echo "<div style=\"background-color:{$color};height:100px\">Ejemplo</div>"; 
    }
}

function ejercicio13v1($colores){
    foreach ($colores as $color) {
    ?>
       <div style="background-color:<?=$color?>;height:100px">Ejemplo</div>; 
    <?php
    }
}
    ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       
        <?php
        ejercicio13(["red","blue","green"]);
        ejercicio13v1(["red","blue","green"]);
        ?>
    </body>
</html>
