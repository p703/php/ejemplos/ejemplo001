<!DOCTYPE html>
<?php
function ejercicio15($numeros){
    return array_product($numeros);
}

function ejercicio15v1($numeros){
    $resultado=1;
    foreach ($numeros as $numero) {
        $resultado=$resultado*$numero;
    }
    return $resultado;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo ejercicio15([2,3,4]);
        echo ejercicio15v1([3,4,5]);
        ?>
    </body>
</html>
