<!DOCTYPE html>
<?php
/**
 * Version inicial
 
function ejercicio9($numeros){
    $negativo=false;
    foreach ($numeros as $valor) {
        if($valor<0){
            $negativo=true;
        }
    } 
    if($negativo==true){
        return "Hay negativos";
    }else{
        return "Todos positivos";
    }
}*/


/**
 * Con cambios
 */
function ejercicio9($numeros){
    $negativo=false;
    foreach ($numeros as $valor) {
        if($valor<0){
            $negativo=true;
            break;
        }
    } 
    if($negativo==true){
        return "Hay negativos";
    }else{
        return "Todos positivos";
    }
}
/**
 * Metodo mas corto
 
function ejercicio9($numeros){
    foreach ($numeros as $valor) {
        if($valor<0){
            return "Hay negativos";
        }
    } 

        return "Todos positivos";
    
}*/


/**
 * Metodo mas corto mejorado
 
function ejercicio9($numeros){
    $mensaje="Todos positivos";
    foreach ($numeros as $valor) {
        if($valor<0){
            $mensaje="Hay negativos";
        break;
        }
    } 

        return $mensaje;
    
}*/

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo "<br/>" . ejercicio9([1,2,3,4]) . "<br/>";
        echo "<br/>" . ejercicio9([-1,2,-2,3]) . "<br/>";
        ?>
    </body>
</html>
