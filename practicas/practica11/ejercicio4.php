<?php
/**
 * Funcion de genera colores
 * @param int $numero El numero de colores a generar
 * @param bool $almohadilla=true con valor true nos indica que coloquemos la almohadilla
 * @return array los colores solicitados en un array de cadenas
 */

function generaColores($numero,$almohadilla=true){
    $colores=array();
    for($n=0;$n<$numero;$n++){//FOR: inicializacion, mantenimiento, incremento
        $c=0;//contador para el segundo bucle (mejor ponerlo en el bucle)
        $limite=6;
        $colores[$n]="";//primer color a calcular
        if($almohadilla){
            $colores[$n]="#";
            $limite=7;
        }
        for(;$c<$limite;$c++){//falta $c antes de ; porque está puesto arriba (linea 12)
            $colores[$n].=dechex(mt_rand(0,15));
        }
    }
    return $colores;
}
$salida=generaColores(5);
$salida1= generaColores(4,false);
var_dump($salida,$salida1);
