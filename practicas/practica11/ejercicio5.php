<?php
/**
 * Funcion que le pasa un array con valores y ella te devuelve las veces que se repite cada valor
 * @param int $array es el conjunto de valores a utilizar
 * @param int $devolverTodos si le indicas true te devuelve los valores que no se repiten tambien
 * @return int[] es el array con las frecuencias de cada uno de los valores de array de entrada
 */

function elementosRepetidos($array,$devolverTodos=false){
    $repeated=array();
    //$array=[1,1,2,3,2,1];
    
    foreach ((array)$array as $value){
        $inArray=false;
        
        foreach($repeated as $i=>$rItem){
            if($rItem['value']===$value){
                $inArray=true;
                ++$repeated[$i]['count'];
            }
        }
        
        if(false===$inArray){//comprueba el tipo de dato con 3=
            //$i=count($repeated);//para saber el ultimo elemento que he metido
            //$repeated[$i]=array();
            //array_push($repeat,[]);//para meter un valor en $repeat
            //$repeated[]=[];
            //$repeated[$i]['value']=$value; //valor
            //$repeated[$i]['count']=1;//cuenta
            $repeated[]=[
                'valor'=>$value,
                'cuenta'=>1
            ];
        }
    }
    if(!$devolverTodos){
        foreach($repeated as $i=>$rItem){
            if($rItem['count']===1){
                unset($repeated[$i]);//unset=funcion para romper variables especificadas
            }
        }
    }
    
    sort($repeated);
    
    return $repeated;
}

//$entrada=array(1,2,3,"a","a","b",1,1,1,1,1);
$entrada=[1,1,2,2,1,4];
var_dump($elementosRepetidos($entrada,TRUE));

$a=[1,2,3];
$a[]=[2,3];
