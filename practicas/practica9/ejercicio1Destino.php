<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .tabla{
                display:table;
                width:200px;
            }
            
            .fila{
                display:table-row;
            }
            .fila>div{
                display:table-cell;
                border:1px solid black;
            }
        </style>
    </head>
    <body>

        <div class="tabla">
        <div class="fila">
            <div>
                Nombre  
            </div>
            <div>
                <?= $_GET["nombre"] ?>
            </div>
        </div>
            <div class="fila">
            <div>
                Color  
            </div>
            <div>
                <?= $_GET["color"] ?>
            </div>
        </div>
        <div class="fila">
            <div>
                Alto  
            </div>
            <div>
                <?= $_GET["alto"] ?>
            </div>
        </div>
        <div class="fila">
            <div>
                Peso  
            </div>
            <div>
                <?= $_GET["peso"] ?>
            </div>
        </div>
        
        
        <table border="1" width="400" cellspacing="0">
            <tr>
                <td>Nombre</td>
                <td><?= $_GET["nombre"] ?></td>
            </tr>
            <tr>
                <td>Color</td>
                <td><?= $_GET["color"] ?></td>
            </tr>
            <tr>
                <td>Alto</td>
                <td><?= $_GET["alto"] ?></td>
            </tr>
            <tr>
                <td>Peso</td>
                <td><?= $_GET["peso"] ?></td>
            </tr>
        </table>
        
    </body>
</html>
