<?php
define("RUTA","http://127.0.0.1/poo2021/php/practicas/practica10"); //ruta donde está guardado este archivo
if(empty($_REQUEST)){ //pregunta si se ha rellenado el formulario (la otra forma es:!$_REQUEST)
    header("Location: " . RUTA . "/ejercicio5.php");//redirecciona a esta pagina si no rellenan el formulario
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        extract($_REQUEST); //recoge lo que hay en el formulario en "nombre" y 
        //crea el solo la variable para no tener que declararla tu
        //Así sería sin el 'extract':
        //$nombre=$_REQUEST["nombre"];
        echo $nombre; //Muestra lo que ha recogido el formulario que contiene esta variable
        ?>
    </body>
</html>
