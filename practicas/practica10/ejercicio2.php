<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $datos = array(20, 40, 790, 234, 890);
        $todo = max($datos);
        foreach ($datos as $key => $value){
            $datos[$key] = (int) (100 * $datos[$key] / $todo);
        }
        var_dump($datos);
        // C:\xampp\htdocs\poo2021\php\practicas\practica10\ejercicio2.php:14:
        // array (size=5)
        // 0 => int 2
        // 1 => int 4
        // 2 => int 88
        // 3 => int 26
        // 4 => int 100
        // Hasta aquí el var_dump recorre el array para mostrarlo 
        // haciendo la operacion que contiene el foreach
        
        
        foreach ($datos as $value){
            for ($c = 0; $c < $value; $c++){
                echo "*";
            }
            echo "<br>";
        }
        //**
        //****
        //****************************************************************************************
        //**************************
        //****************************************************************************************************
        //Esto muestra los valores de la operacion contenida en 
        //el foreach anterior, transformados a porcentajes y a su vez en asteriscos
        ?>
    </body>
</html>
