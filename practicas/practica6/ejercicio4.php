<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // esto es inutil
        $s;
        $j;
        $k;
        $i;
        // inicializa la variable
        $a = 2.345;
        $s = 0;
        
        // bucle
        for ($j = 1; $j <= 4; $j++){
            echo $j; // mostrar 1234
            if ($j % 2 == 0){ // si el valor de j es par
                $s+=$j; // (s=s+j) s=0+2+4 ==> $s tendria que valer 6
            }
        }
        //printf("<br>%d", $j);
        echo "<br>$j"; // el valor de j cuando sale del bucle es 5
        
        var_dump($s); // el valor de s es 0 
        
        // bucle
        $i = 10; // contador 
        while ($i > 0){
            $i = $i - 1; // $i ==> 10 9 8 7 6 5 4 3 2 1 0
        }
        
        echo $i; // i vale 0
        
        echo gettype($i) . "<br>"; // esta funcion me indica el tipo de dato que es i (integer)
        
        // estas dos funciones son para pruebas
        print_r($i); // esta es para arrays
        var_dump($a); // esta vale para todo
       
        ?>
    </body>
</html>


