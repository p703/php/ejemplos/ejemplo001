<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <ul>
            <?php
            $colores = [
                'blue', 'yellow', 'green', 'red', 'black'
            ];

            foreach ($colores as $indice=>$valor){
            ?>
            <li><?= $valor ?></li>
            <?php
            }
            ?>

        </ul>
        <?php
        foreach ($colores as $indice=>$valor){
            ?>
            <div style="width:100px;height:100px;background-color: <?= $valor ?>" ></div>
            <?php
        }
        ?>
    </body>
</html>