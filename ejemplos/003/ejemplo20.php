<?php
const MIN=1; // las constantes siempre en mayusculas y sin $
            // podria usarse esto: define (MIN,1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            if(!$_GET) {// comprobando si se pulsa BUTTON
            //if (empty ($_GET)){
            //if(!isset($_GET["numero"])){                                   
            
        ?>
        
        <form method="get">
            <div>
            <label>Introduce el numero</label>
            <input type="number" name="numero" min="2" required>
            </div>
            <button>Mostrar torre</button>
        </form>
        
        
        
        <?php
            }else{
            //aqui entra cuando he pulsado BUTTON
            
            $numero=$_GET["numero"];
            
            echo "<ul>"; //fuera de los bucles. Esto solo se ejecuta una vez
            for($contador=MIN;$contador<$numero;$contador++){ //este bucle le controla numero
                echo "<li>"; // esto se ejecuta al principio de cada iteracion del bucle 1
                for($contador1=MIN;$contador1<=$contador;$contador1++){ //este bucle le controla contador
                    echo "{$contador1} "; //esto se ejecuta en cada iteracion del bucle 2
                }
                echo "</li>"; //esto se ejecuta al final de cada iteracion del bucle 1
            }
            echo "</ul>";
            }
        ?>
    </body>
</html>
