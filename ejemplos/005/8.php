<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function calcularArea($r){
            $resultado=0;
            $resultado=pi()*$r**2;//** para elevar al cuadrado
            return $resultado;
        }
        
        function calcularAreaReferencia($r,&$areaCalculada){
            $areaCalculada=pi()*$r**2;
        }
        
        
        echo "<br>Area de un circulo <br>";
        $radio=2;
        $area=0;
        $area=calcularArea($radio);
        var_dump($area);
        
        echo "<br>Area de un circulo por referencia <br>";
        $radio=5;
        calcularAreaReferencia($radio,$area);
        var_dump($area);
        ?>
        
    </body>
</html>
