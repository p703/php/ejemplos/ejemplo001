<?php
function operacionConRetorno($a,$b){
    //en una sola instruccion
    $c=[
        "suma"=> $a+$b,
        "resta"=> $a-$b,
        "producto"=> $a*$b
    ];
    //en tres instrucciones
    //$c["suma"]=$a+$b;
    //$c["resta"]=$a-$b;
    //$c["producto"]=$a*$b;
    
    return $c;
}

function operacionReferencia($a,$b,&$c){
    //inicializando c y almacenando los resultados
    $c=[
        "suma"=> $a+$b,
        "resta"=> $a-$b,
        "producto"=> $a*$b
    ];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <?php
            $numero1=20;
            $numero2=10;
            $resultado=[];
            $resultado=operacionConRetorno($numero1,$numero2);
            var_dump($resultado);
            $resultado=[];
            operacionReferencia($numero1, $numero2, $resultado);
            var_dump ($resultado);
        ?>
    </body>
</html>
