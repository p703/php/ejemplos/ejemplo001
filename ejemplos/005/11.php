<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
            Realizar una funcion que calcule la moda de un array pasado como argumento
            El array utilizado es:
            $x=[1,2,3,4,1,2,3,4,1,1];
            La sintaxis de la funcion es:
            int moda(array a utilizar)
        </pre>
        <?php
        function moda($vector){
            $repeticiones= array_count_values($vector);
            $repeticionMaxima=max($repeticiones);
            $resultado=array_search($repeticionMaxima, $repeticiones);
            return $resultado;
        }
        
        
        
        function moda1($vector){
            $repeticiones= array_count_values($vector);
            $modaCalculada=0;
            $repeticionesMaximas=0;
            
            foreach($repeticiones as $indice=>$valor){
                if($valor>$repeticionesMaximas){
                    $repeticionesMaximas=$valor;
                    $modaCalculada=$indice;
                    
                }
            }
            return $modaCalculada;
        }
        
        $x=[1,2,3,4,1,2,3,4,1,1];
        $salida=0;
        $salida=moda1($x);
        var_dump($salida);
        
        
        
        ?>
    </body>
</html>
