<?php
function operacion($a,$b,$c){
    $c=$a+$b;//La suma que realizo no sirve para nada 
}

function operacionConRetorno($a,$b){
    $c=$a+$b;
    return $c;//(para devolver) devuelvo la suma
}

function operacionReferencia($a,$b,&$c){
    $c=$a+$b;//lo pasa de forma bidireccional
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <?php
            $numero1=20;
            $numero2=10;
            $resultado=0;
            operacion($numero1,$numero2,$resultado);//$a=20,$b=10,$c=0
            echo $resultado;//vale 0
            $resultado=operacionConRetorno($numero1,$numero2);
            echo $resultado;// vale 30
            $resultado=0;
            operacionReferencia($numero1, $numero2, $resultado);//$numero1=>$a,$numero2=>$b(por valor)|$resultado=>$c(bidireccional porque es un argumento por referencia)Lo que cambio a $c se cambia a $resultado
            echo $resultado;//vale 30
        ?>
    </body>
</html>
