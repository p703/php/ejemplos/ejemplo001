<?php
   function __autoload($classname) {
        $filename = "./ejemplo4/". $classname .".php";
        include_once($filename);
    }

?>

<?php
    $alumno=new Persona();
    $perro=new Animal();
    
    $alumno->setNombre("Andres");
    echo "Mi alumno se llama " . $alumno->getNombre();
    
    $perro->setTipo("perro");
    $perro->setNombre("Tor");
    
    $perro->datos();
    
?>
