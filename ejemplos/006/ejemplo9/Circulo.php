<?php
namespace ejemplo9;

/**
 * Description of Circulo
 *
 * @author Alpe
 */
class Circulo {
    private $radio;
    private $centrox;
    private $centroy;
    
    public function __construct($radio=50, $centrox=25, $centroy=25) {
        $this->radio = $radio;
        $this->centrox = $centrox;
        $this->centroy = $centroy;
    }

    public function getRadio() {
        return $this->radio;
    }

    public function getCentrox() {
        return $this->centrox;
    }

    public function getCentroy() {
        return $this->centroy;
    }

    public function setRadio($radio): void {
        $this->radio = $radio;
    }

    public function setCentrox($centrox): void {
        $this->centrox = $centrox;
    }

    public function setCentroy($centroy): void {
        $this->centroy = $centroy;
    }

        public function Area(){
        $resultado=0;
        $resultado= pi()*$this->radio**2;
        return $resultado;
    }
    
    public function Perimetro(){
        $resultado=0;
        $resultado=2*pi()*$this->radio;
        return $resultado;
    }
    
    public function pintar(){
        echo '<svg height="400" width="400">';
        echo '<circle cx="'. $this->centrox .'" cy="'. $this->centroy .'" r="'. $this->radio .'" stroke="black" stroke-width="3" fill="red" />';
        echo '</svg>';
    }
}
