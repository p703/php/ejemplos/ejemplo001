<!DOCTYPE html>
<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
use ejemplo9\Circulo;
?>

<?php
$unCirculo=new Circulo(3.4,2,2);
echo "<br>Area " . $unCirculo->Area();
echo "<br>Perímetro " . $unCirculo->Perimetro();
echo $unCirculo->getRadio();//get para sacar cosas
$unCirculo->pintar();

$unCirculo1=new Circulo(15,30,50);
$unCirculo1->pintar();
?>

<?php
//CODIGO DE ANTONIO:
/**
    // Autocarga
    spl_autoload_register(function ($clase) {
        include $clase . '.php';
    });
    
    // Espacio de nombres
    use ejemplo9\Circulo;
?>

<html lang='es'>
    <head>
        <meta charset='UTF-8'>
        <title>Ejemplo 9</title>
        <style>
            img {
                display: inline;
                clear: left;
                width: 25%;
            }
            clearboth {
                clear: both;
            }
        </style>
    </head>
    <body>
        <h1>Ejemplo 9</h1>
        <h2>UML</h2>
        <img src='ejemplo9/img/Circulo.PNG' alt='Circulo' title='Circulo'>
        <img src='ejemplo9/img/Nota.PNG' alt='Nota' title='Nota'>
        <div class='clearboth' />
        
        <h2>Resultados</h2>
        <?php
            // Creamos objeto de la clase Circulo
            $unCirculo = new Circulo(3.4, 2.0, 2.0);
            
            // Llamamos a los métodos de la clase Circulo
            $radio = number_format(((float) $unCirculo->getRadio()), 1);
            $area = number_format(((float) $unCirculo->Area()), 1);
            $perimetro = number_format(((float) $unCirculo->Perimetro()), 1);
        
?>
        <h3>Circulo de radio <?= $radio ?></h3>
        <ul>
            <li><b>Área</b>: <?= $area ?></li>
            <li><b>Perímetro</b>: <?= $perimetro ?></li>
        </ul>
    </body>
</html>
*/


