<?php


/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
class Persona {
    public $nombre;
    
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }
}

