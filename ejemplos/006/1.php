<?php
//creando una clase
    class Persona{
        //propiedades publica
        public $nombre=null;
        public $apellido=null;
        public $edad;
        
        //propiedad privada
        private $tratamiento="Sr/a";
        
        
        //metodo publico
        
        //getter
        public function getNombre(){
            return $this->tratamiento . " " . $this->nombre; //$this->nombre;=>para acceder a tus miembros
        }
        
        //setter
        public function setNombre($nombre){
            $this->nombre = strtoupper($nombre);
        }

        public function nombreCompleto(){
            return $this->getNombre() . " " . $this->apellido;
        }
        
        public function datos(){
            echo "<br>Nombre: " . $this->nombre; 
            echo "<br>Apellido: " . $this->apellido;
            echo "<br>Edad: " . $this->edad;
            echo "<br> Iniciales: " . $this->calcularIniciales();
        }
        
        //metodo privado (interno para mi)
        private function calcularIniciales(){
            return $this->nombre[0] . ". " . $this->apellido[0] . ". ";
            //return substr($this->nombre,0,1) . ". " . substr($this->apellido,0,1) . ". "; Otra forma de hacerlo
        }
        
        
        
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //echo $alumno1->nombre; Sin()=>propiedades
        //echo $alumno1->getNombre(); Con()=>metodos
        
        //crear un objeto
        //instanciar
        $persona1=new Persona();
        var_dump($persona1);//persona1 es objeto
        //var_dump($Persona); no hacer esto: Persona es la clase
        
        //Introducir datos en persona1
        $persona1->nombre="Susana";
        $persona1->apellido="Lopez";
        
        //mostrando los datos
        $persona1->datos();
        
        //mostrando el nombre
        echo "<br>" . $persona1->getNombre();
        
        //introduciendo el nombre a traves del setter
        $persona1->setNombre("Susana");
        
        //mostrando el nombre
        echo "<br>" . $persona1->getNombre();
        
        
        ?>
    </body>
</html>
