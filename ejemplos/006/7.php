<?php
    require_once "./ejemplo7/Animal7.php";
    require_once "./ejemplo7/Ciudad7.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //quiero utilizar la clase
        //creamos un objeto de tipo animal
        
        $animal1=new Animal();
        $animal1->setNombre("miau");
        $animal1->setRaza("siames");
        $animal1->setColor("gris");
        $animal1->setFechaNacimiento("2021/01/01");
        
        //mostrar la informacion introducida
        echo $animal1->getNombre();
        
        echo $animal1->datos();
        
        //quiero crear otro objeto
        $animal2=new Animal();
        $animal2->setNombre("guau");
        $animal2->setRaza("mastin");
        $animal2->setColor("negro");
        $animal2->setFechaNacimiento("2020/01/01");
        
        //mostrar la informacion introducida
        echo $animal2->getNombre();
        
        
        //crear dos ciudades
        $ciudad1=new Ciudad();
        $ciudad1->setNombre("Santander");
        $ciudad1->setProvincia("Cantabria");
        
        echo $ciudad1->getNombre();
        echo "<div>" . $ciudad1->obtenerIniciales(5) . "</div>";
        
        $ciudad2=new Ciudad();
        $ciudad2->setNombre("Oviedo");
        $ciudad2->setProvincia("Asturias");
        
        echo $ciudad2->getNombre();
       
        ?>
    </body>
</html>
