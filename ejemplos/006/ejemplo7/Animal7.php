<?php
/**
 * Description of Animal
 *
 * @author Alpe
 */

class Animal {
    //propiedades
    public $nombre;
    private $raza;
    protected $color;
    public $fechaNacimiento;
    
    //metodos
    public function getNombre() {
        return $this->nombre;
    }

    public function getRaza() {
        return $this->raza;
    }

    public function getColor() {
        return $this->color;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    public function setRaza($raza): void {
        $this->raza = $raza;
    }

    public function setColor($color): void {
        $this->color = $color;
    }

    public function setFechaNacimiento($fechaNacimiento): void {
        $this->fechaNacimiento = $fechaNacimiento;
    }
    
    public function datos(){
        $resultado="<ul>";
        $resultado.="<li>{$this->nombre}</li>";
        $resultado.="<li>{$this->raza}</li>";
        $resultado.="<li>{$this->color}</li>";
        $resultado.="</ul>";
        
        return $resultado;
    }
}

